package com.venus.kafka.util.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/kafka")
    public String Kafka(){
        return "/index";
    }

    @GetMapping("/index")
    public String Index(){
        return "/index";
    }

    @GetMapping("/message")
    public String Message(){
        return "/message";
    }
}
