package com.venus.kafka.util.service;

import com.alibaba.fastjson.JSON;
import com.venus.kafka.util.response.ResponseResult;
import com.venus.kafka.util.response.enums.RestResultEnum;
import com.venus.kafka.util.serviceApi.ConsumerApi;
import com.venus.kafka.util.vo.KafkaBody;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class Consumer implements ConsumerApi {


//    @Value("${test}")

    public static void kafkaConsumer() {
        String topic="test";
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "192.168.8.54:9092");
        properties.put("group.id", "Demo-group");
        properties.put("enable.auto.commit", false);
        properties.put("auto.commit.interval.ms", "5000");
        properties.put("auto.offset.reset", "earliest");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<String, String>(properties);
        List<TopicPartition> tp =new ArrayList<TopicPartition>();

        Collection<PartitionInfo> partitionInfos = kafkaConsumer.partitionsFor(topic);
        partitionInfos.forEach(str -> {
            System.out.println("Partition Info:");
            System.out.println(str);
            tp.add(new TopicPartition(topic,str.partition()));
            kafkaConsumer.assign(tp);
            kafkaConsumer.seekToEnd(tp);
            System.out.println("Partition " + str.partition() + " 's latest offset is '" + kafkaConsumer.position(new TopicPartition(topic, str.partition())));
        });
        try {
            TopicPartition partition0 = new TopicPartition(topic, 1);
            kafkaConsumer.assign(Arrays.asList(partition0));
            kafkaConsumer.seek(partition0,2L);
            int countuid = 0;
            List idList = new ArrayList();
            ConsumerRecords<String, String> records = kafkaConsumer.poll(1000000);
            long lantency = 0L;
            int i = 0;
            for (ConsumerRecord<String, String> record : records) {
//                JSONObject jsonObject = JSONObject.fromObject(record.value());
//                if (jsonObject.getString("uid").equals("5e995779000000000100b2b9")){
//                    countuid++;
//                    System.out.println("-----------------");
//                    System.out.printf("offset = %d, value = %s", record.offset(), record.value());
//                    System.out.println();
//                }
//                JSONObject results = jsonObject.getJSONObject("result");
//                JSONArray note_list = results.getJSONArray("note_list");
                Long time = Long.valueOf(record.value().split(",")[0]);
                lantency += (System.currentTimeMillis() - time);
                i = i+1;

//                JSONArray note_list = jsonObject.getJSONArray("result");
//                for (Object result : note_list ){
//                    JSONObject res = JSONObject.fromObject(result);
//                    String s = res.getString("create_time");
//                    s = s.substring(0, s.length() - 3);
//                    Integer createtime = Integer.parseInt(s);
//
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
////                    String time_Date = sdf.format(new Date(createtime));
//                    String time1="2020-03-12 00:00:00";
//                    Integer timenow = Integer.parseInt(String.valueOf(sdf.parse(time1).getTime()/1000));
//                    String time2="2020-05-26 00:00:00";
//                    Integer timemor = Integer.parseInt(String.valueOf(sdf.parse(time2).getTime()/1000));
//                    if (createtime >= timenow && createtime < timemor){
//                        System.out.println("-----------------");
//                        System.out.printf("offset = %d, value = %s", record.offset(), record.value());
//                        System.out.println();
//                    }
////                    idList.add(res.getString("id"));
//                }
            }
            String avgLantency = String.valueOf(lantency / i);
            System.out.println(avgLantency+"ms");
//            System.out.print("note_list所含id数，去重前  "+idList.size());
//            System.out.println();
//            //去重
//            for( int i=0; i<idList.size()-1; i++){
//                for( int j=idList.size()-1 ; j>i; j--)  {
//                    if(idList.get(j).equals(idList.get(i))){
//                        idList.remove(j);
//                    }
//                }
//            }
//            kafkaConsumer.commitAsync();
            System.out.print("用户数："+countuid);
            System.out.println();
            System.out.print("note_list所含id数  "+idList.size());
            System.out.println();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            kafkaConsumer.close();
        }
    }

    @Override
    public KafkaBody getDownMessage(KafkaBody kafkaBody) {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", kafkaBody.getIp());
        properties.put("group.id", kafkaBody.getGroup());
        properties.put("enable.auto.commit", false);
        properties.put("auto.commit.interval.ms", "5000");
        properties.put("auto.offset.reset", "earliest");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<String, String>(properties);
        List<TopicPartition> tp =new ArrayList<TopicPartition>();
        String topic = kafkaBody.getTopic();
        KafkaBody kafkaBody1 = new KafkaBody();
        try {
            TopicPartition partition0 = new TopicPartition(topic, kafkaBody.getPartition());
            kafkaConsumer.assign(Arrays.asList(partition0));
            kafkaConsumer.seek(partition0, kafkaBody.getOffset());
            ConsumerRecords<String, String> records = kafkaConsumer.poll(1000000);
            int i = 0;
            List<Map<String, Object>> list = new ArrayList<>();
            for (ConsumerRecord<String, String> record : records) {
                Map<String, Object> map = new HashMap<>();
                if (i ==0 && record.offset() > kafkaBody.getOffset()){
                    kafkaBody1.setContent(Collections.singletonList("该topic未消费offset起始值为" + record.offset()+"，若需改变offset，请使用kafka命令更改当前offset（仅测试环境可用）"));
                    return kafkaBody1;
                }
                if (i==kafkaBody.getSize()){
                    kafkaConsumer.close();
                    return kafkaBody1;
                }
                map.put("offset", String.valueOf(record.offset()));
                map.put("value", JSON.parseObject(record.value()));
                i = i+1;
                list.add(map);
                kafkaBody1.setContentJson(list);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            kafkaConsumer.close();
        }
        return kafkaBody1;
    }

    @Override
    public KafkaBody getDownMessageNoOffset(KafkaBody kafkaBody) {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", kafkaBody.getIp());
        properties.put("group.id", kafkaBody.getGroup());
        properties.put("enable.auto.commit", false);
        properties.put("auto.commit.interval.ms", "5000");
        properties.put("auto.offset.reset", "earliest");
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<String, String>(properties);
        List<TopicPartition> tp =new ArrayList<TopicPartition>();
        String topic = kafkaBody.getTopic();
        KafkaBody kafkaBody1 = new KafkaBody();
        try {
            TopicPartition partition0 = new TopicPartition(topic, kafkaBody.getPartition());
            kafkaConsumer.assign(Arrays.asList(partition0));
            kafkaConsumer.seek(partition0, kafkaBody.getOffset());
            ConsumerRecords<String, String> records = kafkaConsumer.poll(1000000);
            int i = 0;
            ArrayList<String> arrayList = new ArrayList<>();
            for (ConsumerRecord<String, String> record : records) {
                if (i ==0 && record.offset() > kafkaBody.getOffset()){
                    kafkaBody1.setContent(Collections.singletonList("该topic未消费offset起始值为" + record.offset()));
                    return kafkaBody1;
                }
                if (i==kafkaBody.getSize()){
                    kafkaConsumer.close();
                    return kafkaBody1;
                }
                arrayList.add(record.value());
                kafkaBody1.setContent(arrayList);
                i = i+1;
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            kafkaConsumer.close();
        }
        return kafkaBody1;
    }
}