<#import "/spring.ftl" as s>
<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <meta charset="utf-8">
    <title>kafka数据处理平台</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <#include 'include/baseCss.ftl'>
</head>

<body class="dashboard-page">
<div id="main">
    <header id="vueheader" class="navbar navbar-fixed-top">
        <div class="navbar-branding dark">
            <a class="navbar-brand">
                <b>kafka数据处理平台</b>
            </a>
        </div>
        <button class="btn btn-sm btn-primary" style="float: right; font-weight: bold; margin-right: 60px; margin-top: 16px" @click="goBack">
            <span>返回</span>
        </button>
    </header>

    <section id="content_wrapper" style="margin-top: 40px">
        <section id="content">
            <div class="col-md-12">
                <table class="table table-bordered" v-if="details.contentJson == 0" >
                    <tr>
                        <th>{{details.content}}</th>
                    </tr>
                </table>
                <table class="table table-bordered" v-for="detail in details.contentJson" >
                    <tr>
                        <th>offset:{{detail.offset}}</th>
                    </tr>
                    <tr>
                        <th>
                            <textarea style=" width: 100%; min-height: 300px;">{{detail.value}}</textarea>
                        </th>
                    </tr>
                </table>
            </div>
        </section>
    </section>
</div>
<#include 'include/baseJs.ftl'/>
<script>
    var app = new Vue({
        el: '#main',
        data: {
            down:{
                ip:'',
                group:'',
                topic:'',
                partition:'',
                offset:'',
                size:'',
                message:'',
                sendTopic:'',
                broker:''
            },
            details:{}
        },
        created: function () {
            this.downMessage();
        },
        mounted: function () {

        },
        watch: {},
        methods: {
            goBack:function () {
                window.location.href='kafka';
            },
            downMessage:function () {
                var url1 = location.search;
                if ( url1.indexOf( "?" ) != -1 ) {
                    var str = url1.substr( 1 );
                    var strs = str.split( "," );
                    this.down.ip = ( strs[ 0 ] );
                    this.down.group = ( strs[ 1 ] );
                    this.down.topic = ( strs[ 2 ] );
                    this.down.partition = ( strs[ 3 ] );
                    this.down.offset = ( strs[ 4 ] );
                    this.down.size = ( strs[ 5 ] );
                    this.down.broker = ( strs[6]);
                    console.log( this.down );
                }
                var url = "/api/v1/down";
                this.$http.post(url, this.down).then(function (response) {
                    this.details = response.data.data;
                    if (this.details.content !== null){
                        this.details.contentJson = 0;
                    }
                    console.log(this.details.contentJson);
                },function (error) {
                    swal(error.body.msg);
                })
            }
        }
    })
</script>
</body>
</html>
