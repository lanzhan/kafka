<#import "/spring.ftl" as s>
<!DOCTYPE html>
<html lang="zh_CN">
<head>
    <meta charset="utf-8">
    <title>kafka数据处理平台</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <#include 'include/baseCss.ftl'>
</head>

<body class="dashboard-page">
<div id="main">
    <header id="vueheader" class="navbar navbar-fixed-top">
        <div class="navbar-branding dark">
            <a class="navbar-brand">
                <b>kafka数据处理平台</b>
            </a>
        </div>
    </header>
    <section id="content_wrapper" style="margin-top: 40px">
        <section id="content">

            <div class="tray tray-center">
                <div class="panel" id="spy7">
                    <div class="panel-heading">
                        <span class="panel-title" style="float: right; font-weight: bold; color: #2a74d6" data-toggle="modal" data-target="#helpModal">帮助</span>
                    </div>
                    <div class="panel-body">
                        <div class="panel col-md-12">
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th>
                                                <input id="item1" type="radio" name="item" v-model="down.ip" value="1">线上
                                                <input id="item2" type="radio" name="item" v-model="down.ip" value="2" checked="checked">测试
<#--                                                <input style="width:120px; height: 30px" v-model="down.broker" disabled="true">-->
                                                <SELECT id="idState" v-model="down.broker" style="visibility: hidden">
                                                    <OPTION value="1" selected="selected">192.168.12.7</OPTION>
                                                    <OPTION value="2">192.168.12.8</OPTION>
                                                    <OPTION value="3">192.168.12.9</OPTION>
                                                </SELECT>
                                            </th>
                                            <th>
                                                group:
                                                <input style="width:120px; height: 30px" v-model="down.group">
                                            </th>
                                            <th>
                                                topic:
                                                <input style="width:120px; height: 30px" v-model="down.topic">
                                            </th>
                                            <th>
                                                partition:
                                                <input style="width:120px; height: 30px" v-model="down.partition">
                                            </th>
                                            <th>
                                                offset:
                                                <input style="width:120px; height: 30px" v-model="down.offset">
                                            </th>
                                            <th>
                                                条数:
                                                <input style="width:120px; height: 30px" v-model="down.size">
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center" colspan="6">
                                                <button class="btn btn-sm btn-primary" @click="downMessage">
                                                    <span>查询</span>
                                                </button>
                                                <button class="btn btn-sm btn-primary" @click="sendMessage">
                                                    <span>发送消息</span>
                                                </button>
                                                <button class="btn btn-sm btn-primary" data-toggle="modal" data-target="#downSendModal">
                                                    <span>线上数据获取并上传</span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center" colspan="6">
                                                <textarea style=" width: 100%; min-height: 300px;" v-model="down.message"></textarea>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- send模态框（Modal） -->
            <div class="modal fade" id="downSendModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">目标topic</h4>
                        </div>
                        <div class="modal-body">
                            <tr>
                                <th>
                                    sendTopic:
                                    <input style="width:120px; height: 30px" v-model="down.sendTopic">
                                </th>
                            </tr>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                            <button type="button" class="btn btn-primary" @click="downAndSend">提交</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- help模态框（Modal） -->
            <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <table class="table table-bordered">
                                    <tr>
                                        <th style="font-weight: bold">
                                            一、查询：
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>须选择所要查询的环境（线上192.168.12.6/测试192.168.8.54），
                                        group（例：Demo-group），topic，partition，offset，及条数</td>
                                    </tr>
                                    <tr>
                                        <td>—>>注：若offset值超出当前topic最大offset值，则自动为您查找最早offset指定条数</td>
                                    </tr>
                                    <tr>
                                        <th style="font-weight: bold">
                                            二、发送消息：
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>须填写topic及内容（下方文本框处填写），默认ip：测试192.168.8.54。该功能为单条数据导入，文本框内数据默认视为一条</td>
                                    </tr>
                                    <tr>
                                        <th style="font-weight: bold">
                                            三、线上数据获取并上传：
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>该功能须填写查询消息需要的各个字段（数据来源），点击"线上数据获取并上传按钮"后，填写需要上传到的topic</td>
                                    </tr>
                                </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </section>
</div>
<#include 'include/baseJs.ftl'/>
<script>
    var app = new Vue({
        el: '#main',
        data: {
            down:{
                ip:'2',
                group:'Demo-group',
                topic:'',
                partition:'',
                offset:'',
                size:'',
                message:'',
                sendTopic:'',
                broker:''
            },
            details:{}
        },
        created: function () {
            this.$watch("down.ip", function (newVal) {
                if(this.down.ip=='2'){
                    document.getElementById("idState").style.visibility="hidden";
                }else if(this.down.ip=='1'){
                    this.down.group='mbase_spider_consumer';
                    document.getElementById("idState").style.visibility="visible";
                }
            })
        },
        mounted: function () {

        },
        watch: {},
        methods: {
            downMessage:function () {
                window.location.href='message?'+this.down.ip+','+this.down.group+','+this.down.topic+
                    ','+this.down.partition+','+this.down.offset+','+this.down.size+','+this.down.broker;
            },
            sendMessage:function () {
                var url = "/api/v1/send";
                this.$http.post(url, this.down).then(function (response) {
                    this.details = response.data.data;
                    swal(this.details.message);
                },function (error) {
                    swal(error.body.msg);
                })
            },
            downAndSend:function () {
                var url = "/api/v1/downAndSend";
                this.$http.post(url, this.down).then(function (response) {
                    this.details = response.data.data;
                    swal(this.details.message);
                },function (error) {
                    swal(error.body.msg);
                })
            }
        }
    })
</script>
</body>
</html>
