package com.venus.kafka.util.vo;

import java.util.Map;

import lombok.Data;

import java.util.List;

@Data
public class KafkaBody {

    private String topic;

    private String sendTopic;

    private String group;

    private Integer partition;

    private Long offset;

    private String ip;

    private Integer size;

    private String message;

    private List<String> content;

    private List<Map<String, Object>> contentJson;

    private String broker;
}
