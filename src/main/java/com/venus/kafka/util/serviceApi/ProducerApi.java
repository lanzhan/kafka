package com.venus.kafka.util.serviceApi;

import com.venus.kafka.util.vo.KafkaBody;

import java.util.List;

public interface ProducerApi {

    KafkaBody getSendMessage(KafkaBody kafkaBody);

    KafkaBody getSendListMessage(KafkaBody kafkaBody);
}
