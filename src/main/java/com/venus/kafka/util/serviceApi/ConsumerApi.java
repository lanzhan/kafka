package com.venus.kafka.util.serviceApi;

import com.venus.kafka.util.vo.KafkaBody;

public interface ConsumerApi {

    KafkaBody getDownMessage(KafkaBody kafkaBody);

    KafkaBody getDownMessageNoOffset(KafkaBody kafkaBody);
}
