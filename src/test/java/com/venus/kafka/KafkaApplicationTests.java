package com.venus.kafka;

import com.venus.kafka.util.service.Consumer;
import com.venus.kafka.util.service.Producer;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class KafkaApplicationTests {

    @Test
    public void testConsumer() {
        Consumer.kafkaConsumer();
    }

    @Test
    public void testProducer() {
        String data = String.format("1232332",1);
        Producer.send(data);
    }


}
