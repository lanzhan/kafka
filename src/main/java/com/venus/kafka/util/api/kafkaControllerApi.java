package com.venus.kafka.util.api;

import com.venus.kafka.util.response.ResponseResult;
import com.venus.kafka.util.service.Partitions;
import com.venus.kafka.util.serviceApi.ConsumerApi;
import com.venus.kafka.util.serviceApi.ProducerApi;
import com.venus.kafka.util.vo.KafkaBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/api/v1")
public class kafkaControllerApi {

    @Resource
    private ConsumerApi consumerApi;

    @Autowired
    private ProducerApi producerApi;

    @PostMapping("/down")
    public ResponseResult<KafkaBody> getDownMessage(@RequestBody(required = false) KafkaBody kafkaBody){
        brokers(kafkaBody);
        KafkaBody downMassage = consumerApi.getDownMessage(kafkaBody);
        return new ResponseResult<>(downMassage);
    }

    @PostMapping("/send")
    public ResponseResult<KafkaBody> getSendMessage(@RequestBody(required = false) KafkaBody kafkaBody){
        KafkaBody sendMessage = new KafkaBody();
        if (kafkaBody.getMessage().equals("") || kafkaBody.getTopic().equals("") || kafkaBody.getMessage() == null){
            sendMessage.setMessage("消息体或topic不能为空");
            return new ResponseResult<>(sendMessage);
        }
        brokers(kafkaBody);
        sendMessage = producerApi.getSendMessage(kafkaBody);
        return new ResponseResult<>(sendMessage);
    }

    @PostMapping("/downAndSend")
    public ResponseResult<KafkaBody> downAndSend(@RequestBody(required = false) KafkaBody kafkaBody){
        brokers(kafkaBody);
        KafkaBody contentList = consumerApi.getDownMessageNoOffset(kafkaBody);
        kafkaBody.setContent(contentList.getContent());
        KafkaBody sendMessage = producerApi.getSendListMessage(kafkaBody);
        return new ResponseResult<>(sendMessage);
    }

    private void brokers(@RequestBody(required = false) KafkaBody kafkaBody) {
        if(kafkaBody.getIp().equals("1")){
            if(kafkaBody.getBroker().equals("1")){
                kafkaBody.setIp("192.168.12.7:9092");
            }else if(kafkaBody.getBroker().equals("2")){
                kafkaBody.setIp("192.168.12.8:9092");
            }else if(kafkaBody.getBroker().equals("3")){
                kafkaBody.setIp("192.168.12.9:9092");
            }
        }else if(kafkaBody.getIp().equals("2")){
            kafkaBody.setIp("192.168.8.54:9092");
        }
    }
}
