<link rel="stylesheet" type="text/css" href="<@s.url '/assets/css/admin-forms.css'/>">
<link rel="stylesheet" type="text/css" href="<@s.url '/assets/css/theme.css'/>">
<link rel="stylesheet" type="text/css" href="<@s.url '/assets/css/sweetalert.css'/>">
<link rel="stylesheet" type="text/css" href="<@s.url '/assets/plugins/toastr/toastr.min.css'/>">
<#--<link href="<@s.url '/assets/plugins/bootstrap/css/bootstrap.css'/>" rel="stylesheet" type="text/css">-->
<link rel="stylesheet" type="text/css" href="<@s.url '/assets/plugins/bootstrap3-dialog/css/bootstrap-dialog.css'/>">

<script src="<@s.url '/assets/plugins/sweetAlert/sweetalert.min.js'/>"></script>

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

<![endif]-->
