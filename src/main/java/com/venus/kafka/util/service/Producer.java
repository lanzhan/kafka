package com.venus.kafka.util.service;

import com.alibaba.fastjson.JSON;
import com.venus.kafka.util.serviceApi.ProducerApi;
import com.venus.kafka.util.vo.KafkaBody;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Slf4j
//@ConfigurationProperties(prefix = "application")
//@PropertySource(value = { "classpath:config/application.yml" })
@Service
public class Producer implements ProducerApi {


    private static KafkaProducer<String,String> producer;


    /**
     * 发送kafka消息
     */
    public static void send(String data) {
        Properties props = new Properties();
        String topic="test";
        props.put("bootstrap.servers", "192.168.8.54:9092");
        props.put("acks", "all");//所有follower都响应了才认为消息提交成功，即"committed"
        props.put("retries", 0);//retries = MAX 无限重试，直到你意识到出现了问题:)
        props.put("batch.size", 16384);//producer将试图批处理消息记录，以减少请求次数.默认的批量处理消息字节数
        //batch.size当批量的数据大小达到设定值后，就会立即发送，不顾下面的linger.ms
        props.put("linger.ms", 1);//延迟1ms发送，这项设置将通过增加小的延迟来完成--即，不是立即发送一条记录，producer将会等待给定的延迟时间以允许其他消息记录发送，这些消息记录可以批量处理
        props.put("buffer.memory", 33554432);//producer可以用来缓存数据的内存大小。
        props.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//        List<String> interceptors = new ArrayList<>();
//        interceptors.add("com.venus.kafka.util.service.AddTimeStampInterceptor");
//        props.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, interceptors);
//        props.put("partitioner.class", "com.venus.kafka.util.Partitions");

        producer = new KafkaProducer<String, String>(props);
          producer.send(new ProducerRecord<String, String>(topic, data), new Callback(){

              @Override
              public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                  if (recordMetadata != null) {
                      System.err.println(recordMetadata.partition() + "---" + recordMetadata.offset());
                  }

              }
          });
        producer.close();
    }


    @Override
    public KafkaBody getSendMessage(KafkaBody kafkaBody) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "192.168.8.54:9092");
        props.put("acks", "all");//所有follower都响应了才认为消息提交成功，即"committed"
        props.put("retries", 0);//retries = MAX 无限重试，直到你意识到出现了问题:)
        props.put("batch.size", 16384);//producer将试图批处理消息记录，以减少请求次数.默认的批量处理消息字节数
        //batch.size当批量的数据大小达到设定值后，就会立即发送，不顾下面的linger.ms
        props.put("linger.ms", 1);//延迟1ms发送，这项设置将通过增加小的延迟来完成--即，不是立即发送一条记录，producer将会等待给定的延迟时间以允许其他消息记录发送，这些消息记录可以批量处理
        props.put("buffer.memory", 33554432);//producer可以用来缓存数据的内存大小。
        props.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//        props.put("partitioner.class", "com.venus.kafka.util.service.Partitions");
        KafkaBody kafkaBody1 = new KafkaBody();
        kafkaBody1.setMessage("发送失败，请校验后重新发送");
        producer = new KafkaProducer<String, String>(props);
        Future<RecordMetadata> send = producer.send(new ProducerRecord<String, String>(kafkaBody.getTopic(), kafkaBody.getMessage()), new Callback() {
            @Override
            public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                if (recordMetadata != null) {
                    System.err.println(recordMetadata.partition() + "---" + recordMetadata.offset());
                }
            }
        });
        try {
            kafkaBody1.setMessage("分区" + send.get().partition() + "---offset" + send.get().offset());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        producer.close();
        return kafkaBody1;
    }

    @Override
    public KafkaBody getSendListMessage(KafkaBody kafkaBody) {
        Properties props = new Properties();
        props.put("bootstrap.servers", "192.168.8.54:9092");
        props.put("acks", "all");//所有follower都响应了才认为消息提交成功，即"committed"
        props.put("retries", 0);//retries = MAX 无限重试，直到你意识到出现了问题:)
        props.put("batch.size", 16384);//producer将试图批处理消息记录，以减少请求次数.默认的批量处理消息字节数
        //batch.size当批量的数据大小达到设定值后，就会立即发送，不顾下面的linger.ms
        props.put("linger.ms", 1);//延迟1ms发送，这项设置将通过增加小的延迟来完成--即，不是立即发送一条记录，producer将会等待给定的延迟时间以允许其他消息记录发送，这些消息记录可以批量处理
        props.put("buffer.memory", 33554432);//producer可以用来缓存数据的内存大小。
        props.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//        props.put("partitioner.class", "com.venus.kafka.util.service.Partitions");
        KafkaBody kafkaBody2 = new KafkaBody();
        kafkaBody2.setMessage("发送失败，请校验后重新发送");
        producer = new KafkaProducer<String, String>(props);
        int i=0;
        if (kafkaBody.getContent().get(0).contains("该topic未消费offset起始值为")){
            kafkaBody2.setMessage(kafkaBody.getContent().get(0));
            return kafkaBody2;
        }else{
            for (String message: kafkaBody.getContent()) {
                Future<RecordMetadata> send = producer.send(new ProducerRecord<String, String>(kafkaBody.getSendTopic(), message), new Callback() {
                    @Override
                    public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                        if (recordMetadata != null) {
                            System.err.println(recordMetadata.partition() + "---" + recordMetadata.offset());
                        }
                    }
                });
                i = i+1;
            }
            kafkaBody2.setMessage("成功"+i+"条");
        }
        producer.close();
        return kafkaBody2;
    }
}
